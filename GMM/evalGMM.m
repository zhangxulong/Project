function p=evalGMM(data,prior,mu,sigma)
    nGMM=size(sigma,3);
    %[nVar,nDim] = size(data);
    [D,N] = size(data);
    %pComp=zeros(nGMM,1);
    for i=1:nGMM
        tdata = data' - repmat(mu(:,i)',N,1);
        %data = data' - repmat(mu(:,i)',N,1);
%p = sum((data*inv(sigma)).*data, 2);
        ptemp = sum((tdata/sigma(:,:,i)).*tdata, 2);
        %ptemp = ptemp/(sum(ptemp));
        pComp(:,i) = prior(i)*exp(-0.5*ptemp) / sqrt((2*pi)^D * (abs(det(sigma(:,:,1)))+realmin));  
    end
    p=sum(pComp);