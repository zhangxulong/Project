function result=testSample(filenumber)
% This function test a sample recording and returns whether it contains a
% whale or not.
labels=csvread('train_wo_headers.csv',0,1);
% load GMM parameters
load('GMM.mat');
%extract feature vector from test sample
filename=['data/test/' int2str(filenumber) '.aif'];
fprintf('File %i ',filenumber);
fV=extractFeatureVector(filename);
if ~isempty(fV)
    result=classifyfV(fV,g1,g2);
else
    fprintf(' \n');
    result=0;
end
%plot results
if labels(filenumber)==1
    ltext='Whale Present';
else
    ltext='Whale Not Present';
end
if result
    wtext='Whale Detected';
else
    wtext='Whale Not Detected';
end
screen_size = get(0, 'ScreenSize');
f1=figure;
set(f1, 'Position', [10 150 screen_size(3)/2 screen_size(4)/1.5 ] );
pcolor(fV);
title(sprintf('%s And %s ',ltext,wtext),'FontWeight','bold')