function [g1 g2]=trainGMM
clc
clear
delete('GMM.mat')
% Gaussian Mixture Models Training with Expectation-Maximumization Algorithm
%
% This function trains 2 GMMs using the EM algoritm. The 2 GMMs are Whale
% Present, g1, and No Whale Present, g2. The input to the function are
% labeled audio recordings in .aiff format labeled as Whale Present or No
% Whale Present. From these recordings, audio spectrum feature vectors are
% extracted using a modified Mel-Frequency Cesptral Coefficient approach.
% The first 6 modified MFCC vectors are then used as input to the EM
% algorithm to estimate the parameters of the GMM: mu, sigma, weighting
% coefficients (prior probabilities). The GMM parameter estimates for each
% model are initialized using kmeans clustering from 1 recording with a
% whale and 1 recording without a whale respectively. The trained GMMs are
% then saved and used in the testing phase.
%
% Key Variables:
%       fV - feature vector extracted from recording
%       g1 - GMM structure for whale, has fields
%                   - mu    = mean
%                   - sigma = covariance matrix
%                   - prior = prior probabilities, weighting coeff for GMM
%       g2 - GMM structure for no whale, has fields
%                   - mu    = mean
%                   - sigma = covariance matrix
%                   - prior = prior probabilities, weighting coeff for GMM
%

%load labels to identify recordings
labels=csvread('train_wo_headers.csv',0,1);
nGMM=12;
% if no model input
if nargin==0
    % initialize GMM for Whale, g1
    filename='whale.aif';
    %filename='C:\School\Pattern Recogition\data\test\test12.aiff';
    fV=extractFeatureVector(filename);
    %disp(fV);
    [prior1, mu1, sigma1] = init_EM(fV, nGMM);
    %[prior1, mu1, sigma1] = EM_init_kmeans(fV, nGMM);
    clear fV
    % initialize GMM for No Whale,g1
    filename='nowhale.aif';
    fV=extractFeatureVector(filename);
    [prior2, mu2, sigma2] = init_EM(fV, nGMM);
    %[prior2, mu2, sigma2] = EM_init_kmeans(fV, nGMM);
    clear fV
end
%loop through aiff files and train GMMs
nTrainingSamples=10;
whaleCount=1;
nowhaleCount=1;
for i=3:nTrainingSamples
    % check label on recording
    if labels(i)==1;
        whalePresent=1;
        fprintf('File %i Whale \n',i);
    else
        whalePresent=0;
        fprintf('File %i No Whale \n',i);
    end
    % extract feature vector from recording
    filename=['data/test/' int2str(i) '.aif'];
    if i==38
        x=1;
    end
    disp(filename);
    fV=extractFeatureVector(filename);
    % train GMM for whale and no whale recordings
    if ~isempty(fV)
        if whalePresent
            %train Whale GMM with EM Algorithm
            %g1=trainEM(fV, g1);
            %fprintf(' Whale \n');
            [g1(whaleCount).prior, g1(whaleCount).mu, g1(whaleCount).sigma] = EM_Algo(fV, prior1, mu1, sigma1);
            clear fV
            whaleCount=whaleCount+1;
        else
            %train No Whale GMM with EM Algorithm
            %g2=trainEM(fV, g2);
            %fprintf(' No Whale \n');
            [g2(nowhaleCount).prior, g2(nowhaleCount).mu, g2(nowhaleCount).sigma] = EM_Algo(fV, prior2, mu2, sigma2);
            % check if update parameters are NaNs
            clear fV
            nowhaleCount=nowhaleCount+1;
        end
    end
end
% %save GMM for testing
% g1.prior=prior1;
% g1.mu=mu1;
% g1.sigma=sigma1;
%
% g2.prior=prior2;
% g2.mu=mu2;
% g2.sigma=sigma2;
%save results for testing phase
save('GMM.mat','g1','g2');

% data=cc_n(1:5,:);
% [prior, mu, sigma] = EM_init_kmeans(data, 3);
% [prior, mu, sigma, Pix] = EM(data, prior, mu, sigma);
% p=evalGMM(data,prior,mu,sigma)



