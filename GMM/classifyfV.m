function result=classifyfV(fV,g1,g2)
p1=[];
p2=[];
for j=1:length(g1)
    p1=[p1;evalGMM(fV,g1(j).prior,g1(j).mu,g1(j).sigma)];
end
for j=1:length(g2)
    p2=[p2;evalGMM(fV,g2(j).prior,g2(j).mu,g2(j).sigma)];
end
% OR IF P for both is really low, label as whale not present!
if sum(p1)>sum(p2)
    fprintf(' Whale detected by 2nd Stage \n');
    result=1; return;% whale is present
else
    fprintf(' \n');
    result=0; return; % whale is not present
end