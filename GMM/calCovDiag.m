function sigma=calCovDiag(Data)
n=size(Data,1);
sigma=zeros(n,n);
for i=1:n
    sigma(i,i)=var(Data(i,:));
end