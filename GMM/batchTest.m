function results=batchTest
% Batch testing for sample recordings
clc
labels=csvread('train_wo_headers.csv',0,1);
istart=3;
istop=10;
results=zeros((istop-istart+1),5); %stores results for ID rate calc
%test each sample and record result
i=1;
for ii=istart:istop
    results(i,1)=testSample(ii);
    results(i,2)=labels(ii);
    if results(i,1)~=results(i,2)
        % if classification error, classify as miss or false alarm
        if results(i,2)==0
            results(i,4)=1; %false alarm
        else
            results(i,5)=1; %miss
        end
    else
        %if correct classification, set result 3 to 1
        results(i,3)=1;
    end
    i=i+1;
end
idrate=sum(results(:,3))/size(results,1); fprintf('\n ID rate %f \n',idrate);
falseAlarmRate=sum(results(:,4))/size(results,1);
fprintf('FA rate %f \n',falseAlarmRate);
missRate=sum(results(:,5))/size(results,1);
fprintf('Miss rate %f \n',missRate);
x=1;