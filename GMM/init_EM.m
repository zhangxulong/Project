function [weights, mu, sigma] = init_EM(data, nGMM)
%
% This function initializes the parameters of a Gaussian Mixture Model
% (GMM) by using k-means clustering algorithm.
%
% Author:	Sylvain Calinon, 2009
%			http://programming-by-demonstration.org
%
% Inputs -----------------------------------------------------------------
%   o Data:     D x N array representing N datapoints of D dimensions.
%   o nbStates: Number K of GMM components.
% Outputs ----------------------------------------------------------------
%   o Priors:   1 x K array representing the prior probabilities of the
%               K GMM components.
%   o Mu:       D x K array representing the centers of the K GMM components.
%   o Sigma:    D x D x K array representing the covariance matrices of the
%               K GMM components.
[n, nbData] = size(data);
%%Use of the 'kmeans' function from the MATLAB Statistics toolbox
%[Data_id, Centers] = kmeans(Data', nbStates);
%Mu = Centers';
[clusternum, mu] = litekmeans(data, nGMM);
%Mu = Centers;
weights=zeros(1,nGMM);
sigma=zeros(n,n,nGMM);
for i=1:nGMM
    tindex = find(clusternum==i);
    weights(i) = length(tindex);
    sigma(:,:,i) = calCovDiag(data(:,tindex))+ 1E-3.*diag(ones(n,1));%cov([Data(:,idtmp) Data(:,idtmp)]');
end
weights = weights ./ sum(weights);