function fV=extractFeatureVector(filename)
%read aiff file
x=double(audioread(filename));
x=real(x(:,1));
n=length(x);
i_common1=0; i_common2=0;
%constants and initialze spectrum vector
nfft=512;
nt=20;
fs=2000;
dt=1/fs;
nf=nfft/2+1;
tt=dt*(nfft-1)/2:nt*dt:(n-1)*dt-(nfft/2)*dt;
ntt=length(tt);
y=zeros(nf,ntt);
s_energy=zeros(1,ntt);
b = [0.0940    0.3759    0.5639    0.3759    0.0940];
a =[1.0000   -0.0000    0.4860   -0.0000    0.0177];
x=filter(b,a,x);
% spectrogram of signal
xw=(0:nfft-1)';
wind=.5*(1-cos((xw*2*pi)/(nfft-1))); %window
for i=1:ntt
    %fft
    zi=(i-1)*nt+1:nfft*i-(i-1)*(nfft-nt);
    xss=fft(x(zi).*wind,nfft)/nfft;
    yy=2*abs(xss(1:(nfft/2)+1));
    s_energy(i)=sum(abs(x(zi).^2));
    y(:,i)=yy;
end
clear x
%remove silent periods and noise from spectrum
noise_threshold=1.2*mean(s_energy);
i_max=0;
ii_max=0;
ii_max2=0;
s_max=0;
ii=1;
y_n=[];
i_n=[];
for i=1:ntt
    if s_energy(i)>noise_threshold
        if s_energy(i)>s_max
            s_max=s_energy(i);
            ii_max2=ii_max;
            i_max=ii;
            ii_max=ii;
        end
        %y_n(:,ii)=y(:,i);
        i_n(ii)=i;
        %f_n(:,ii)=f(:,i);
        %t_n(:,ii)=t(:,i);
        ii=ii+1;
    end
end
if isempty(i_n)
    fV=[];
    return;
else
    y_n=y(:,i_n);
end
clear y
% %t=ones(nf,1)*(1:ntt);
% set(gca,'YTickLabel',[200 400 600 800 1000]);
% xlabel('Frame Number');
% ylabel('Frequency (Hz)');
% shading flat
%calculate modified MFCC
n_MFCC=25;
n_CC=12;
ntt=size(y_n,2);
nff=size(y_n,1);
nn=round(nff/n_MFCC);
%x_c=linspace(0,1000,nn); %approx freq centers
xw=[0:nn-1]';
L=length(xw)*2;
%L=nn;
twind=zeros(1,L);
freq_max_c=zeros(1,n_MFCC);
%create triangular window function, change to constant later
if mod(L,2)
    for i=1:L
        if i<((L+1)/2+1)
            twind(i)=2*i/(L+1);
        else
            twind(i)=2-(2*i)/(L+1);
        end
    end
else
    %twind=[0.1,0.3,0.5,0.7,0.9,0.9,0.7, 0.5,.3 .1];
    for i=1:L
        if i<((L)/2+1)
            twind(i)=(2*i-1)/L;
        else
            twind(i)=2-(2*i-1)/L;
        end
    end
end
%wind=.5*(1-cos((xw*2*pi)/(nn-1)));
c=zeros(n_MFCC-1,ntt);
cc=zeros(n_CC,ntt);
for j=1:ntt
    for i=1:n_MFCC-1
        ii=(nn*(i-1)+1):(nn*(i-1)+L);
        %iii=((i-1)*(L/2)+1):((i-1)*(L/2)+L)
        y_temp=y_n(ii,j).*twind';
        c(i,j) =sum(y_temp);
        %twind_x(:,i)=ii;
        %twind_y(:,i)=twind;
        % c(i,j)=log10(sum_wind);
        %c(i,j)=(sum_wind);
    end
    %     twind_x=4*twind_x;
    %     figure; hold on;
    %     for i=1:size(twind_y,2)
    %         if mod(i,2)
    %             plot(twind_x(:,i),twind_y(:,i),'r');
    %         else
    %             plot(twind_x(:,i),twind_y(:,i),'b');
    %         end
    %     end
    %     xlabel('Frequency (Hz)');
    %     ylabel('Magnitude');
    %Cepstral analysis
    for ci=1:n_CC
        sum_CC=0; dCC=0;
        for i=1:15%n_MFCC-1
            %dCC=log10(c(i,j))*cos(ci*(i-.5)*pi/(n_MFCC-1));
            if i==1
                w=1/sqrt((n_MFCC-1));
            else
                w=sqrt(2/(n_MFCC-1));
            end
            dCC=w*log10(c(i,j))*cos(pi*(2*i-1)*(ci-1)/(2*(n_MFCC-1)));
            sum_CC=sum_CC+dCC;
        end
        cc(ci,j)=sum_CC;
    end
    %most frequent max cc
    try
        i_max_c=find(c(:,j)==max(c(:,j)));
        freq_max_c(1,i_max_c)=freq_max_c(1,i_max_c)+1;
    catch error
        disp(error);
    end
end
%clear c
% if signal is all noise, exit
if isempty(cc)||(size(cc,2)<2)
    %disp('Whale not found')
    fV=[];
    return;
end
% %first stage classifer
% %Check 4th and 5th filter bacnk c_n with max energy
% c_threshold=4.5*mean(mean(c));
% if ((c(7,ii_max)>c_threshold)||(c(6,ii_max)>c_threshold))&&(c(8,ii_max)<c_threshold)  %and not 5 or 6
%     disp('The white whale')
% end
% figure; plot(c(:,ii_max));hold on; plot([0 size(c,1)],[c_threshold c_threshold]);
%find top two most common frequencies
i_common1=find(freq_max_c==max(freq_max_c));
freq_max_c(i_common1)=0;
i_common2=find(freq_max_c==max(freq_max_c));
if ((i_common1(1)==7)&&(i_common2(1)==6))||((i_common1(1)==5)&&(i_common2(1)==6))
    disp('The white whale NEW')
end
close all
%    plot([0 size(c,1)],[c_threshold c_threshold]);
% demean and return feature vector, for better convergence of EM
%fV=cc(1:3,:)-repmat(mean(cc(1:3,:),2)',ntt,1)';
cc=cc-repmat(mean(cc,2)',ntt,1)';
fV=cc;%(1:10,:);